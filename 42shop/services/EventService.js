import axios from 'axios'

const apiClient = axios.create(
    {
        baseURL: 'http://localhost:8001',
        withCredentials: true,
        headers: {
            Accept: 'applicaton/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
            "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token"
        }
    })

export default {
    getProducts(){
        return apiClient.get('/products')
    },
    getProduct(id) {
        return apiClient.get('/product/' + id)
      },
    trylogin(data) {
        return apiClient.post('/login', data);
      },
    getUser(data) {
        return apiClient.get('/user/', data);
    }
}