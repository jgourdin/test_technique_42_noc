const mijin = require('mijin/dist/tailwind-preset.js');
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  presets: [
    mijin,
  ],
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    'node_modules/mijin/src/components/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
      fontFamily: {

    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('daisyui'),
  ],
}
