export const state = () => ({
  allProducts: [],
  cartItems: [],
  user: null,
})
export const getters = {
  /* 
    return items from store
  */
  allProducts: (state) => state.allProducts,
  getUser: (state) => state.user,
  getCart: (state) => state.cartItems,
  getCartItemNumber: (state) => 
    state.cartItems.length < 1
    ? '0'
    : state.cartItems
    .map((el) =>  el.quantity)
    .reduce((a, b) => a + b),
  getCartTotal: (state) =>
    state.cartItems.length < 1
      ? '0'
      : state.cartItems
          .map((el) => el.price * el.quantity)
          .reduce((a, b) => a + b),
}
export const actions = {
  async addItemToCart({ commit }, payload) {
    console.log(payload.quantity);
    await commit('setCartItem', payload)
  },
  async deleteCartItem({ commit }, id) {
    await commit('removeCartItem', id)
  },
}
export const mutations = {
  setProducts: (state, products) => (state.allProducts = products),
  setUser: (state, user) => (state.user = user),
  setCartItem: (state, payload) => {
    let index = state.cartItems.findIndex(cartItem => cartItem.id === payload.item.id);
    if(index > 0)
      state.cartItems[index].quantity += payload.quantity;
    else
      state.cartItems.push({id: payload.item.id, name: payload.item.name, price: payload.item.price, quantity: payload.quantity})
  },
  removeCartItem: (state, id) =>
    state.cartItems.splice(
      state.cartItems.findIndex((el) => el.id === id),
      1
    ),
}