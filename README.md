# Stockoverflow
​
​
Techno obligatoire :
​
- Golang
- Postgresql
- Un micro-framework http (gin, go-swagger, net/http)
- Vue / Nuxt
- Docker
​
Contraintes :
​
- API JSON RESTful obligatoire.
- N'importe quel ORM ou Driver sql possible.
- Environment "Docker-only", pas de supervisord ou autre.
- Librairie libres.
​
### Enonce :
​
Conçois un site e-commerce simple vendant les objets de votre choix.
​
Ce site doit permettre de consulter les articles disponibles, et d'ajouter ces articles dans un panier. L'utilisateur peut à partir de n'importe quelle page et à tout moment s'inscrire à l'aide d'un email/mot de passe et se connecter.
​
​
Une fois connecte l'utilisateur à accès une page récapitulant les articles dans son panier et proposant de finaliser sa commande.
​
​
À la finalisation de la commande, l'utilisateur doit être redirigé vers un récapitulatif de commande et le statut de celle-ci. (faux statut, faux liens vers chronopost ...)
​
​
Un back-office doit permettre à un administrateur de gérer la disponibilité des articles, ou d'en ajouter de nouveau. Mais aussi de consulter les commandes passe par les utilisateurs ainsi que de lister les utilisateurs enregistre.
​
​
### Note
​
Ne pas rendre toutes les fonctionnalités n'est pas éliminatoire, chaque fonctionnalité sera inspectée et notée.
​
Il est possible pour vous d'ajouter des bonus à votre application, n'importe quelle fonctionnalité bonus sera évidemment inspecté et notée. Si vous n'avez pas d'idée en voici quelques-unes :
​
- Catégoriser les articles.
- Statistiques afficher sur la page d'accueil du back-office.
- Panier reserve pendant x minutes.
- Ce qui te passe par la tete.